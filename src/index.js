import React from 'react'
import ReactDOM from 'react-dom'
import './style/index.css'
import App from './component/App'
import reportWebVitals from './reportWebVitals'
import api, { URL } from './api'
import NotFoundPage from './component/page/NotFoundPage'
import {
  BrowserRouter as Router,
} from 'react-router-dom'

api(URL).then(
  json => {
    ReactDOM.render(
      <React.StrictMode>
        <App logements={json}/>
      </React.StrictMode>,
      document.getElementById('root'),
    )
  },
).catch(e => {
  console.error(e)
  ReactDOM.render(
    <React.StrictMode>
      <Router>
        <NotFoundPage/>
      </Router>
    </React.StrictMode>,
    document.getElementById('root'),
  )
})

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
