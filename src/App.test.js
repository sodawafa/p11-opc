import { render, screen } from '@testing-library/react'
import App from './component/App'
import api, { URL } from './api'

let logements = null
describe('Given open my page', () => {
  test('fetches json from an api and fails with 404 message error',
    async () => {
      logements = await api('cc' + URL).then(
        json => {
          return json
        },
      ).catch(e => {
        expect(e.toString()).toBe('TypeError: Problem with JSON: undefined')
      })
    })
  test('fetches json from api GET', async () => {
    logements = await api(URL).then(
      json => {
        return json
      },
    ).catch(e => {
      expect(e).toBeFalsy()
    })
    expect(logements.length).toBe(20)
  })

  test('Then should see my page', async () => {
    render(<App logements={logements}/>)
    const banner = screen.getByText(/Chez vous, partout et ailleurs/i);
    expect(banner).toBeInTheDocument();
    const thumb = screen.getByText(/Appartement cosy/i);
    expect(thumb).toBeInTheDocument();
  })

  /*async () => {*/

  /*document.body.innerHTML = html
  const message = await screen.getByText(/Erreur 404/)
  expect(message).toBeTruthy()*/
})
/*test('fetches bills from an API and fails with 404 message error',
  async () => {
    firebase.get.mockImplementationOnce(() =>
      Promise.reject(new Error('Erreur 404')),
    )
    const html = BillsUI({ error: 'Erreur 404' })
    document.body.innerHTML = html
    const message = await screen.getByText(/Erreur 404/)
    expect(message).toBeTruthy()
  })*/
/*test('test api', () => {
  api(URL).then(
    json => {
      test('renders learn react link', () => {
        render(<App logements={json}/>)
        const linkElement = screen.getByText(/learn react/i);
        expect(linkElement).toBeInTheDocument();
      })
    },
  ).catch(e => {
    console.log(e)
  })

})*/
