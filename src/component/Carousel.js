import React, { Component } from 'react'
import '../style/carousel.css'

class Carousel extends Component {
  constructor (props) {
    super(props)
    this.state = {
      pictures: this.props.pictures,
      counter: 0,
      picture: this.props.pictures[0],
    }
    this.next = this.next.bind(this)
    this.prev = this.prev.bind(this)
  }

  next (e) {
    let counter = this.state.counter + 1
    if (counter >= this.state.pictures.length) {
      counter = 0
    }
    this.setState({
      counter: counter,
      picture: this.state.pictures[counter],
    })
    e.preventDefault()
  }

  prev (e) {
    let counter = this.state.counter - 1
    if (counter < 0) {
      counter = this.state.pictures.length - 1
    }
    this.setState({
      counter: counter,
      picture: this.state.pictures[counter],
    })
    e.preventDefault()

  }

  nextBtn () {
    if (this.state.pictures.length > 1)
      return <button className={'carousel_next'} onClick={this.next}></button>
  }

  prevBtn () {
    if (this.state.pictures.length > 1)
      return <button className={'carousel_prev'} onClick={this.prev}></button>
  }

  render () {
    return <div>
              <div className={'carousel'}
                   style={{ backgroundImage: `url(${this.state.picture})` }}>
                {
                  this.nextBtn()
                }
                {
                  this.prevBtn()
                }
                <div className={'carousel_number'}>
                  {this.state.counter + 1}/{this.state.pictures.length}
                </div>
              </div>
            </div>
  }
}

export default Carousel
