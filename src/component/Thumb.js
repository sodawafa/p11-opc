import React from 'react'
import '../style/thumb.css'
import { Link } from 'react-router-dom'

class Thumb extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      id: this.props.id,
      picture: this.props.picture,
      name: this.props.name
    }
  }

  render () {
    return (
      <Link to={'/logement/' + this.state.id}
            className={'thumb'}
            key={'thumb_' + this.state.id}
            style={{
              backgroundImage: `url(${this.state.picture})`,
            }}>
        <div>
          <p>{this.state.name}</p>
        </div>
      </Link>
    )
  }
}

export default Thumb
