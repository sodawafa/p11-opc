import '../../style/style.css'
import React, { Component } from 'react'
import Thumb from '../Thumb'
import Banner from '../Banner'

class HomePage extends Component {
  constructor (props) {
    super(props)
    this.state = {
      logements: this.props.logements,
    }
  }

  render () {
    return (
      <div>
        <Banner/>
        <div className={'thumbs'}>
          {
            this.state.logements && this.state.logements.map(logement => {
              return (
                <Thumb name={logement.title}
                       picture={logement.cover}
                       key={'thumbs_' + logement.id.toString()}
                       id={logement.id}
                />
              )
            })
          }
        </div>

      </div>
    )
  }

}

export default HomePage


