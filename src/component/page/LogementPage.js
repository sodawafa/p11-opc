import '../../style/style.css'
import React, { Component } from 'react'
import '../../style/Logement.css'
import Tag from '../Tag'
import Collapse from '../Collapse'
import Carousel from '../Carousel'
import Rating from '../Rating'

class LogementPage extends Component {

  constructor (props) {
    super(props)
    this.state = {
      logement: this.props.logement,
    }
  }

  render () {
    return (
      <div className={'container'}>
        <div>
          {
            <Carousel key={'carousels_100'} pictures={this.state.logement.pictures}/>
          }
        </div>
        <div className={'description'}>
          <p>
            {this.state.logement.title}
          </p>
          <p>
            {this.state.logement.location}
          </p>
        </div>
        <div className={'tags'}>
          {
            this.state.logement && this.state.logement.tags.map(
              (tag,index) => {
                return <Tag name={tag} key={'tags_' + index}/>
              })
          }
        </div>
        <div className={'host'}>
          <p>
            {this.state.logement.host.name}
          </p>
          <img src={this.state.logement.host.picture} alt=""/>

        </div>
        <Rating rate={this.state.logement.rating} max={5}/>



        <div className={'collapses'}>
          <Collapse title={'description'}
                    text={this.state.logement.description}
                    key={'collapses_description_' + this.state.logement.id.toString()}
                    _key={'collapse_description_' + this.state.logement.id.toString()}

          />
          <Collapse title={'equipments'}
                    arr={this.state.logement.equipments}
                    key={'collapses_equipments_' + this.state.logement.id.toString()}
                    _key={'collapse_equipments_' + this.state.logement.id.toString()}
          />
        </div>
      </div>
    )
  }

}

export default LogementPage


