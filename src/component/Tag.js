import React from 'react'
import '../style/tag.css'
class Tag extends React.Component{
  constructor (props) {
    super(props)
    this.state = {
      name: this.props.name
    }
  }
  render () {
    return <div className={'tag'}>
      <span className={'text'}>{this.state.name}
      </span>
    </div>
  }
}
export default Tag
