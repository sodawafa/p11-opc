import React, { Component } from 'react'
import '../style/footer.css'
import logo1 from  '../assets/logo1.png'
class Footer extends Component{
  render () {
    return <div className={'footer'}>
      {/*<img src={footer} alt=""/>*/}
      <img src={logo1} alt=""/>
      <p>© 2020 Kasa. All rights reserved</p>
    </div>
  }
}
export default Footer
