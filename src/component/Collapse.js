import React from 'react'
import arrowImg from '../assets/Vector.png'
import '../style/collapse.css'

class Collapse extends React.Component {

  active = true

  constructor (props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
    this.state = {
      title: props.title,
      text: props.text,
      arr: props.arr,
      active: this.active,
    }
  }

  handleClick (e) {
    e.preventDefault()
    this.setState({
      active: !this.state.active,
    })
  }

  ContentText () {
    if (this.state.text !== undefined) {
      return (
        <div key={'collapse_content_text'}>
          <p>
            {this.state.text}
          </p>
        </div>
      )
    }
    return null

  }

  ContentArray () {
    if (this.state.arr !== undefined) {
      return (
        <div>
          <ul>
            {
              this.state.arr && this.state.arr.map(
                  (elem, index) => {
                    return (
                      <li key={'collapse_content_' + index}>{elem}</li>
                    )
                  },
                )
            }
          </ul>
        </div>
      )
    }
    return null
  }

  render () {
    return (
      <article className={`collapse ${this.state.active ? 'active' : ''}`}
               key={this.props._key}
      >
        <header>
          <a href="/#" onClick={this.handleClick}>
            {this.state.title} <img src={arrowImg} alt=""/>
          </a>
        </header>
        {this.state.active ? this.ContentText() : null}
        {this.state.active ? this.ContentArray() : null}

      </article>
    )

  }
}

export default Collapse
