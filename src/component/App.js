import React, { Component } from 'react'
import {
  Switch,
  Route,
  Redirect,
  useParams,
  HashRouter
} from 'react-router-dom'
/*BrowserRouter as Router,useRouteMatch,*/
import '../App.css'
import NotFoundPage from './page/NotFoundPage'
import HomePage from './page/HomePage'
import AboutPage from './page/AboutPage'
import Nav from './Nav'
import Logo from './Logo'
import LogementPage from './page/LogementPage'
import Footer from './Footer'

function LogementPageFunc (props) {
  let { id } = useParams()

  let logement = props.logements.find(x => x.id === id)
  if (logement !== undefined) {
    return <LogementPage logement={logement}/>
  } else {
    return <Redirect to="/error"/>
  }
}

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      logements:this.props.logements
    }
  }

  render () {
    return (
      <HashRouter>
        <main>
          <header>
            <Logo/>
            <Nav/>
          </header>
          <Switch>
            <Route exact path="/">
              <HomePage logements={this.state.logements}/>
            </Route>
            <Route path="/about">
              <AboutPage/>
            </Route>
            <Route path="/logement/:id">
              <LogementPageFunc logements={this.state.logements}/>
            </Route>
            <Route path="*" component={NotFoundPage}/>
          </Switch>
        </main>
        <Footer/>
      </HashRouter>
    )

  }
}

export default App
