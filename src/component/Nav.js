import { Link, useLocation } from 'react-router-dom'
import React from 'react'
import '../style/nav.css'


function getClass (currentPathname, pathname) {
  if (currentPathname === pathname) return 'active'
  return ''
}

function Nav () {

  const location = useLocation()

  return (<nav>
      <ul>
        <li>
          <Link to="/" className={getClass(location.pathname, '/')}>Accueil</Link>
        </li>
        <li>
          <Link to="/about" className={getClass(location.pathname, '/about')}>A Propos</Link>
        </li>
      </ul>
    </nav>
  )

}

export default Nav


