/*export const DISTANT_URL = 'https://s3-eu-west-1.amazonaws.com/course.oc-static.com/projects/Front-End+V2/P9+React+1/logements.json'*/
export const URL = './course.oc-static.com/projects/Front-End+V2/P9+React+1/logements.json'
let api = async function (url) {
  return await fetch(
    url,
    {
      mode: 'no-cors',
      method: 'GET',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      },
    },
  ).then(results => {
    const contentType = results.headers.get('content-type')
    if (results.ok && contentType && contentType.includes('application/json')) {
      return results.json()
    } else {
      throw new TypeError('Problem with JSON: ' + results.error)
    }
  })
}
export default api

